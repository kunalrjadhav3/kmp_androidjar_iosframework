

### What is this repository for? ###

* To consume network APIS in android and Ios using KTOR framework
* Project generates .jar file and .framework file which can be used in android and Ios projects respectively to achieve common codebase using Kotlin multiplatform.  	

### Set up for Android ###

* Download and sync this project in Android Studio
* The Kotlin plugin 1.3.50 or higher should be installed in the IDE
* After syncing project with gradle, click on Make-project hammer icon to generate SharedCode-android.jar file in SharedCode->build->libs folder.
* Consume this .jar file in any android project as a library

### Set up for Ios ###

* A macOS host operating system is required to generate the .framework file in Android Studio IDE
* The Kotlin plugin 1.3.50 or higher should be installed in the IDE
* Download and sync this project in Android Studio
* Run the packForXcode Gradle task of the SharedCode project. We can do it from the Gradle tab in AndroidStudio
* The .framework file is generated to the SharedCode/build/xcode-frameworks folder
* Consume this .framework file in any Ios project as a library
